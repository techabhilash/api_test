import MySQLdb

def connecting_to_data_base(database_host, database_user_name, database_password, data_base):
	db = MySQLdb.connect(host=database_host, user=database_user_name, passwd=database_password,  db=data_base)
	return db

def fetching_hotel_name(cur, property_id):
	cur.execute("SELECT hotel_name FROM sm_hotel_info where hotel_id = %s",[property_id])
	data =  cur.fetchall()
	return data[0][0]

def fetching_hotel_website(cur, property_id):
	cur.execute("SELECT attribute_value from sm_hotel_custom_attributes where hotel_id = %s and attribute_id = '11'",[property_id])	
	data = cur.fetchall()
	return data[0][0]

def fetching_hotel_email(cur, property_id):
	cur.execute("SELECT hotel_email from sm_hotel_email where hotel_id = %s",[property_id])
	data = cur.fetchall()
	return data

def fetching_room_data(cur, property_id):
	cur.execute("SELECT room_type_id,room_type_name,long_description,max_adults,max_children,max_guests,size,room_size_attribute_name from sm_hotel_room where hotel_id = %s and version_no = '0'",[property_id])
	data = cur.fetchall()
	return data

def fetching_phone_number(cur, property_id):
	cur.execute("SELECT hotel_phone,additional_attribute from sm_hotel_phone where hotel_id = %s",[property_id])
	data = cur.fetchall()
	return data

def fetching_room_amenities(cur, room_id):
	cur.execute("SELECT attribute_name from sm_hotel_room_attributes where hotel_room_attribute_id in (SELECT attribute_id from sm_room_attributes where room_type_id = %s)",[room_id])
	data = cur.fetchall()
	return data

def feching_hotel_amenities(cur, property_id):
	cur.execute("SELECT value from sm_hotel_amenities where hotel_id = %s",[property_id])
	data = cur.fetchall()
	return data

def feching_logo_name_and_alt_text(cur, property_id):
	cur.execute("SELECT img_src,alt_text from sm_picture_info where picture_id = (select parent_picture_id from sm_cropped_picture_info where picture_id = (select logo_id from sm_hotel_logo_info where hotel_id = %s))",[property_id]) 
	data = cur.fetchall()
	return data

def feching_favicon_name(cur, property_id):
	cur.execute("SELECT img_src from sm_picture_info where picture_id = (select parent_picture_id from sm_cropped_picture_info where picture_id = (select fav_icon_id from sm_hotel_logo_info where hotel_id = %s))",[property_id]) 
	data = cur.fetchall()
	return data

def feching_hotel_address(cur, property_id):
	cur.execute("SELECT attribute_value from sm_hotel_custom_attributes where hotel_id = %s and attribute_id = '17'",[property_id])
	data = cur.fetchall()
	return data[0][0]

def feching_minimum_and_maximum_child_age(cur, property_id):
	cur.execute("SELECT attribute_value from sm_hotel_custom_attributes where hotel_id = %s and attribute_id in (32,33);",[property_id])
	data = cur.fetchall()
	return data

def feching_hotel_currency(cur, property_id):
	cur.execute("SELECT currency_code from sm_default_currency_list where currency_id = (select attribute_value from sm_hotel_custom_attributes where hotel_id = %s and attribute_id = 9)",[property_id])
	data = cur.fetchall()
	return data[0][0]

def feching_booking_terms_message(cur, property_id):
	cur.execute("SELECT value from sm_translation where translation_id = (select value from sm_message_value where hotel_id = %s and message_type_id = 'BOOKING_TERMS')",[property_id])
	data = cur.fetchall()
	return data[0][0]

def feching_booking_reservation_policy_message(cur, property_id):
	cur.execute("SELECT value from sm_translation where translation_id = (select value from sm_message_value where hotel_id = %s and message_type_id = 'BOOKING_RESERVATION_POLICY')",[property_id])
	data = cur.fetchall()
	return data[0][0]

def feching_booking_message(cur, property_id):
	cur.execute("SELECT value from sm_translation where translation_id = (select value from sm_message_value where hotel_id = %s and message_type_id = 'BOOKING_MESSAGE')",[property_id])
	data = cur.fetchall()
	return data[0][0]

def feching_booking_confirmation_message(cur, property_id):
	cur.execute("SELECT value from sm_translation where translation_id = (select value from sm_message_value where hotel_id = %s and message_type_id = 'BOOKING_CONFIRMATION_MESSAGE')",[property_id])
	data = cur.fetchall()
	return data[0][0]

def feching_advertisement_message_status(cur, property_id):
	cur.execute("SELECT toggle_value from sm_feature_toggle where hotel_id = %s and toggle_name = 'be_advertisement_message';",[property_id])
	data = cur.fetchall()
	return data[0][0]

def feching_email_slider_status(cur, property_id):
	cur.execute("SELECT toggle_value from sm_feature_toggle where hotel_id = %s and toggle_name = 'email_slider';",[property_id])
	data = cur.fetchall()
	return data[0][0]

def fetching_full_payment_choice_status(cur, property_id):
	cur.execute("SELECT toggle_value from sm_feature_toggle where hotel_id = %s and toggle_name ='choice_to_guest_for_pay_now'",[property_id])
	data = cur.fetchall()
	return data[0][0]
def booking_engine_payment_gateway_data(cur, property_id):
        cur.execute("select attribute_name,attribute_value from sm_payment_gateway_attributes where hotel_payment_gateway_id=(select id from sm_payment_gateways where hotel_id= %s) order by attribute_name asc", [
                    property_id])
        data = cur.fetchall()
        #converting the response of database to dictionary format(for easy retrival of the value)
        ret = dict((a,b) for a,b in data)
        return ret

def fetching_paymentgateway_fee(cur, property_id):
    data = booking_engine_payment_gateway_data(cur, property_id)
    return data.get("paymentGatewayFee")

def fetching_simplotel_fee(cur, property_id):
    data = booking_engine_payment_gateway_data(cur, property_id)
    return data.get("simplotelPercentage")

def fetching_google_add_percentage(cur, property_id):
    data = booking_engine_payment_gateway_data(cur, property_id)
    return data.get("googleAdsPercentage")

def fetching_service_tax_percentage(cur, property_id):
    data = booking_engine_payment_gateway_data(cur, property_id)
    return data.get("serviceTaxPercentage")

def fetching_is_before_tax(cur, property_id):
    data = booking_engine_payment_gateway_data(cur, property_id)
    return data.get("beforeTax")

def min_coll_amt_for_pg_fee(cur, property_id):
    data = booking_engine_payment_gateway_data(cur, property_id)
    return data.get("minCollectAmountForPGFee")

def payment_gateway(cur, property_id):
    cur.execute("select payment_gateway_type,payment_gateway from sm_payment_gateways where hotel_id=%s", [
                    property_id])
    data = cur.fetchall()
    return data

def paymentgateway_type(cur, property_id):
    data = payment_gateway(cur, property_id)
    return data[0][0]

def payment_gateway_name(cur, property_id):
    data = payment_gateway(cur, property_id)
    return data[0][1]

def booking_engine_common_data(cur, property_id):
    cur.execute("select attribute_name,attribute_value from sm_booking_engine_attributes where hotel = %s and version_no=0 and booking_engine=100 order by attribute_name asc", [
                    property_id])
    data = cur.fetchall()
#converting the response of database to dictionary format(for easy retrival of the value)
    val = dict((x,y) for x,y in data)
    return val

def booking_url(cur, property_id):
    data = booking_engine_common_data(cur,property_id)
    return data.get("bookingURL")

def booking_engine_type(cur, property_id):
    data = booking_engine_common_data(cur,property_id)
    return data.get("bookingEngineType")

def button_name(cur, property_id):
    data = booking_engine_common_data(cur,property_id)
    return data.get("resBtnName")

def open_new_window(cur, property_id):
    data = booking_engine_common_data(cur,property_id)
    return data.get("openInNewWindow")

def checkin_date(cur, property_id):
    data = booking_engine_common_data(cur,property_id)
    return data.get("checkInDate")

def chain_id(cur, property_id):
    data = booking_engine_common_data(cur,property_id)
    return data.get("chainId")

def show_promocode(cur, property_id):
    data = booking_engine_common_data(cur,property_id)
    return data.get("showPromocode")

def collect_address(cur, property_id):
    data = booking_engine_common_data(cur,property_id)
    return data.get("collectAddress")

def length_of_stay(cur, property_id):
    data = booking_engine_common_data(cur,property_id)
    return data.get("lengthOfStay")

def language_url_parameter(cur, property_id):
    data = booking_engine_common_data(cur,property_id)
    return data.get("languageUrlParameter")

def default_adult(cur, property_id):
    data = booking_engine_common_data(cur,property_id)
    return data.get("adults")

def default_children(cur, property_id):
    data = booking_engine_common_data(cur,property_id)
    return data.get("children")

def max_adult_children(cur,property_id):
    cur.execute("select max(max_adults),max(max_children) from sm_hotel_room where hotel_id=%s and version_no=0", [property_id])
    data = cur.fetchall()
    return data

def max_adult(cur, property_id):
    data = max_adult_children(cur,property_id)
    return data[0][0]

def max_children(cur, property_id):
    data = max_adult_children(cur,property_id)
    return data[0][1]	

def property_Id(cur, property_id):
    data = booking_engine_common_data(cur,property_id)
    return data.get("propertyId")

def is_stop_payout(cur, property_id):
    data = booking_engine_payment_gateway_data(cur,property_id)
    return data.get("stopPayouts")

def is_zero_pg_account(cur, property_id):
    data = booking_engine_payment_gateway_data(cur,property_id)
    return data.get("zeroPGAccount")	
	