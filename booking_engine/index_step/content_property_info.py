import requests
import json
import MySQLdb
import unittest
import sys, os
sys.path.append(os.path.join(os.path.dirname("index_step"), '../..', "config"))
import config
sys.path.append(os.path.join(os.path.dirname("index_step"), '..', "utils"))
import data_base_utils

class Hotel_info_api_test(unittest.TestCase):
	def test_api(self):
		url = config.admin_url+"/content/property/"+str(config.property_id)+"/info"
		r = requests.get(url) 
		print r.status_code
		print r.reason

#------- python dictionary---------------------
		resp_dict = json.loads(r.text)

#--------Data Base Connection-------------------
		db = data_base_utils.connecting_to_data_base(config.database_host, config.database_user_name, config.database_password,	 config.data_base)
		cur = db.cursor()
		
#------- Checking Hotel Name ------------------
		database_hotel_name = data_base_utils.fetching_hotel_name(cur, config.property_id)
		print database_hotel_name
		self.assertEqual(database_hotel_name, resp_dict[''+str(config.property_id)+'']['en-US']['name'])

#------- Checking Hotel Website Name ------------------		
		database_hotel_website_name = data_base_utils.fetching_hotel_website(cur, config.property_id)
		print database_hotel_website_name
		self.assertEqual(database_hotel_website_name, resp_dict[''+str(config.property_id)+'']['en-US']['website'])

#------- Checking Hotel Email ------------------		
		database_hotel_email = data_base_utils.fetching_hotel_email(cur, config.property_id)
		length = len(database_hotel_email)
		i = 0
		for i in range(length):
			print database_hotel_email[i][0]
			self.assertEqual(database_hotel_email[i][0], resp_dict[''+str(config.property_id)+'']['en-US']['emails'][i])	
		
#------- Checking Room Details ------------------
		database_room_detail = data_base_utils.fetching_room_data(cur, config.property_id)
		length = len(database_room_detail)
		i = 0
		for i in range(length):
			j = 0
			print "Room Id :- %s " %database_room_detail[i][j]
			print "Room Name :- %s " %database_room_detail[i][j+1]
			print "Room Detailed Description :- %s " %database_room_detail[i][j+2]
			print "Room Max Adults :- %s " %database_room_detail[i][j+3]
			print "Room Max Children :- %s " %database_room_detail[i][j+4]
			print "Room Max Guest :- %s " %database_room_detail[i][j+5]
			print "Room Max Size :- %s " %database_room_detail[i][j+6]
			print "Room Max Unit :- %s " %database_room_detail[i][j+7]

			self.assertEqual(database_room_detail[i][j], resp_dict[''+str(config.property_id)+'']['en-US']['rooms'][i]['id'])
			self.assertEqual(database_room_detail[i][j+1], resp_dict[''+str(config.property_id)+'']['en-US']['rooms'][i]['name'])
			self.assertEqual(database_room_detail[i][j+2], resp_dict[''+str(config.property_id)+'']['en-US']['rooms'][i]['description'])
			self.assertEqual(database_room_detail[i][j+3], resp_dict[''+str(config.property_id)+'']['en-US']['rooms'][i]['adults'])
			self.assertEqual(database_room_detail[i][j+4], resp_dict[''+str(config.property_id)+'']['en-US']['rooms'][i]['children'])
			self.assertEqual(database_room_detail[i][j+5], resp_dict[''+str(config.property_id)+'']['en-US']['rooms'][i]['guests'])
			self.assertEqual(database_room_detail[i][j+6], resp_dict[''+str(config.property_id)+'']['en-US']['rooms'][i]['size']['area'])
			self.assertEqual(database_room_detail[i][j+7], resp_dict[''+str(config.property_id)+'']['en-US']['rooms'][i]['size']['unit'])

			database_room_aminities = data_base_utils.fetching_room_amenities(cur, database_room_detail[i][j])
			room_aminities_length = len(database_room_aminities)
			k = 0
			room_aminities_array = []
			for k in range(room_aminities_length):
				room_aminities_array.append(database_room_aminities[k][0])

#------- Checking Room aminities ------------------
			for k in range(room_aminities_length):
				print resp_dict[''+str(config.property_id)+'']['en-US']['rooms'][i]['amenities'][k]
		 		if resp_dict[''+str(config.property_id)+'']['en-US']['rooms'][i]['amenities'][k] in room_aminities_array:
		 			print True
		 		else:
		 			print False 

#------- Checking hotel Phone Number ------------------
		database_hotel_phone_number = data_base_utils.fetching_phone_number(cur, config.property_id)
		length = len(database_hotel_phone_number)
		i = 0
		for i in range(length):
			j = 0
			print database_hotel_phone_number[i][j]
			print database_hotel_phone_number[i][j+1]
			self.assertEqual(database_hotel_phone_number[i][j], resp_dict[''+str(config.property_id)+'']['en-US']['phone_numbers'][i]['number'])
			self.assertEqual(database_hotel_phone_number[i][j+1], resp_dict[''+str(config.property_id)+'']['en-US']['phone_numbers'][i]['label'])
		
#--------Checking Hotel Aminities ------------------------
		data_base_hotel_amenities = data_base_utils.feching_hotel_amenities(cur, config.property_id)
		hotel_amenities_length = len(data_base_hotel_amenities)
		hotel_aminities_array = []
		i = 0
		for i in range(hotel_amenities_length):
			hotel_amenities_dict = json.loads(data_base_hotel_amenities[i][0])
			hotel_aminities_array.append(hotel_amenities_dict['name'])
		for i in range(hotel_amenities_length):
			print resp_dict[''+str(config.property_id)+'']['en-US']['amenities'][i]['name']
			if resp_dict[''+str(config.property_id)+'']['en-US']['amenities'][i]['name'] in hotel_aminities_array:
				print True
			else:
				print False

#-------- Checking Porperty Id --------------------------
		print resp_dict[''+str(config.property_id)+'']['en-US']['propertyId']
		self.assertEqual(resp_dict[''+str(config.property_id)+'']['en-US']['propertyId'], config.property_id)


#-------Checking Logo ------------------------------------
		wordList = resp_dict[''+str(config.property_id)+'']['en-US']['logo']['src'].split("/")
		n = len(wordList) 
		Image_Name = wordList[n-1]
		print Image_Name
		data_base_logo_name = data_base_utils.feching_logo_name_and_alt_text(cur,config.property_id)
		wordList_1 = data_base_logo_name[0][0].split("/")
		n = len(wordList_1) 
		Image_Name_1 = wordList_1[n-1]
		Image_Name_1 = Image_Name_1.split(".")
		print Image_Name_1[0]
		self.assertEqual(Image_Name_1[0], Image_Name)
		print resp_dict[''+str(config.property_id)+'']['en-US']['logo']['alt_text']
		self.assertEqual(data_base_logo_name[0][1], resp_dict[''+str(config.property_id)+'']['en-US']['logo']['alt_text'])

#--------Checking Favicon --------------------------------
		wordList = resp_dict[''+str(config.property_id)+'']['en-US']['favicon']['src'].split("/")
		n = len(wordList) 
		Image_Name = wordList[n-1]
		Image_Name = Image_Name.split(".")
		print Image_Name[0]
		data_base_favicon_name = data_base_utils.feching_favicon_name(cur,config.property_id)
		wordList_1 = data_base_favicon_name[0][0].split("/")
		n = len(wordList_1) 
		Image_Name_1 = wordList_1[n-1]
		Image_Name_1 = Image_Name_1.split(".")
		print Image_Name_1[0]
		self.assertEqual(Image_Name_1[0], Image_Name[0])
		print resp_dict[''+str(config.property_id)+'']['en-US']['logo']['alt_text']
		
#-------checking Address --------------------------------
		data_base_hotel_address = data_base_utils.feching_hotel_address(cur, config.property_id)
		print resp_dict[''+str(config.property_id)+'']['en-US']['address']
		self.assertEqual(data_base_hotel_address, resp_dict[''+str(config.property_id)+'']['en-US']['address'])

#-------chicking Minimum and Maximum Child--------------------
		data_base_min_max_child_age = data_base_utils.feching_minimum_and_maximum_child_age(cur, config.property_id)
		print resp_dict[''+str(config.property_id)+'']['en-US']['min_child_age']
		self.assertEqual(data_base_min_max_child_age[0][0], resp_dict[''+str(config.property_id)+'']['en-US']['min_child_age'])
		print resp_dict[''+str(config.property_id)+'']['en-US']['max_child_age']
		self.assertEqual(data_base_min_max_child_age[1][0], resp_dict[''+str(config.property_id)+'']['en-US']['max_child_age'])

#-------Checking Currency -------------------------------------
		data_base_hotel_currency =  data_base_utils.feching_hotel_currency(cur, config.property_id)
		print resp_dict[''+str(config.property_id)+'']['en-US']['currency']
		self.assertEqual(data_base_hotel_currency, resp_dict[''+str(config.property_id)+'']['en-US']['currency'])

#-------checking booking terms message----------------------------------
		data_base_booking_terms_message = data_base_utils.feching_booking_terms_message(cur, config.property_id)
		print resp_dict[''+str(config.property_id)+'']['en-US']['messages']['BOOKING_TERMS']
		self.assertEqual(data_base_booking_terms_message, resp_dict[''+str(config.property_id)+'']['en-US']['messages']['BOOKING_TERMS'])

#-------checking booking reservation policy message----------------------------------
		data_base_booking_reservation_policy_message = data_base_utils.feching_booking_reservation_policy_message(cur, config.property_id)
		print resp_dict[''+str(config.property_id)+'']['en-US']['messages']['BOOKING_RESERVATION_POLICY']
		self.assertEqual(data_base_booking_reservation_policy_message, resp_dict[''+str(config.property_id)+'']['en-US']['messages']['BOOKING_RESERVATION_POLICY'])

#-------checking booking message----------------------------------
		data_base_booking_message = data_base_utils.feching_booking_message(cur, config.property_id)
		print resp_dict[''+str(config.property_id)+'']['en-US']['messages']['BOOKING_MESSAGE']
		self.assertEqual(data_base_booking_message, resp_dict[''+str(config.property_id)+'']['en-US']['messages']['BOOKING_MESSAGE'])

#-------checking booking confirmation message----------------------------------
		data_base_booking_confirmation_message = data_base_utils.feching_booking_confirmation_message(cur, config.property_id)
		print resp_dict[''+str(config.property_id)+'']['en-US']['messages']['BOOKING_CONFIRMATION_MESSAGE']
		self.assertEqual(data_base_booking_confirmation_message, resp_dict[''+str(config.property_id)+'']['en-US']['messages']['BOOKING_CONFIRMATION_MESSAGE'])

#-------chicking advertisement toggle status-----------------------------------
		data_base_advertisement_toggle_status = data_base_utils.feching_advertisement_message_status(cur, config.property_id)
		if (data_base_advertisement_toggle_status == "enabled"):
			data_base_advertisement_toggle_status = True
		else:
			data_base_advertisement_toggle_status = False
		self.assertEqual(data_base_advertisement_toggle_status, resp_dict[''+str(config.property_id)+'']['en-US']['advertisement_message_status'])		
		print resp_dict[''+str(config.property_id)+'']['en-US']['advertisement_message_status']
		print resp_dict[''+str(config.property_id)+'']['en-US']['advertisement_message']

#-------chicking email slider status-----------------------------------
		data_base_email_slider_status = data_base_utils.feching_email_slider_status(cur, config.property_id)
		if (data_base_email_slider_status == "enabled"):
			data_base_email_slider_status = True
		else:
			data_base_email_slider_status = False
		self.assertEqual(data_base_email_slider_status, resp_dict[''+str(config.property_id)+'']['en-US']['email_slider_status'])		
		print resp_dict[''+str(config.property_id)+'']['en-US']['email_slider_status']

		print resp_dict[''+str(config.property_id)+'']['en-US']['country_phone_code']
		print resp_dict[''+str(config.property_id)+'']['en-US']['div_code']
		print resp_dict[''+str(config.property_id)+'']['en-US']['chainId']

		cur.close()
		db.close()

if __name__ == "__main__":
    unittest.main()

	# # print ['propertyId']name
	# url = "http://stagingbe.simplotel.com/availability"
	# payload = {"propertyId":925,"checkIn":"2017-07-16","checkOut":"2017-07-17","rooms":[{"id":1,"adults":1,"children":0}],"isSterling":False,"promocode":"","name":"None","email":"None","phone":"None","step":"None","roomId":"None","ratePlanId":"None"}
	# r = requests.post(url, data=json.dumps(payload))
	# print r.status_code
	# print r.reason
	# print r.text
	# print r["925"]["en-US"].propertyId	
 