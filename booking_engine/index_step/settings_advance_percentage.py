import requests
import json
import MySQLdb
import unittest
import sys, os
import datetime
sys.path.append(os.path.join(os.path.dirname("index_step"), '../..', "config"))
import config
sys.path.append(os.path.join(os.path.dirname("index_step"), '..', "utils"))
import data_base_utils

class Hotel_info_api_test(unittest.TestCase):
	def test_api(self):
		url = config.admin_url+"/settings/advancePercentage?propertyId="+str(config.property_id)+"&checkIn="+str(datetime.date.today())+"&checkOut="+str(datetime.date.today() + datetime.timedelta(days=1))
		print url
		r = requests.get(url) 
		print r.status_code
		print r.reason

		#------- python dictionary-------------------------
		resp_dict = json.loads(r.text)

		#--------Data Base Connection----------------------
		db = data_base_utils.connecting_to_data_base(config.database_host, config.database_user_name, config.database_password,	 config.data_base)
		cur = db.cursor()

		#--------Checking full_payment_choice_status----------
		data_base_full_payment_choice_status = data_base_utils.fetching_full_payment_choice_status(cur, config.property_id)
		if(data_base_full_payment_choice_status == 'enabled'):
			status = True
		else:
			status = False
		print status
		print resp_dict['fullPaymentChoice']
		self.assertEqual(status,resp_dict['fullPaymentChoice'])

		cur.close()
		db.close()

if __name__ == "__main__":
    unittest.main()