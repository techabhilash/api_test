import requests
import json
import MySQLdb
import unittest
import sys
import os
sys.path.append(os.path.join(os.path.dirname("index_step"), '../..', "config"))
import config
sys.path.append(os.path.join(os.path.dirname("index_step"), '..', "utils"))
import data_base_utils

class Hotel_info_api_test(unittest.TestCase):
#====================Starting of the TEST CASE====================================================================        
    def test_api(self):
        property_id = config.property_id
        ref_json_to_compare = {}
        url = config.admin_url + "/settings/?propertyId=" + str(property_id)
        print url
        url_response = requests.get(url)
        print url_response.status_code
        print url_response.reason
        jformat = url_response.json()
        print jformat
        # with open('/tmp/data4.json', 'w') as outfile:
        #     json.dump(jformat, outfile)
        # arr = []
        # for item in jformat:
        #     arr.append(item)
        # print arr
#--------Data Base Connection-------------------
        db = data_base_utils.connecting_to_data_base(config.database_host, config.database_user_name, config.database_password,	 config.data_base)
        cur = db.cursor()

#--------checking whether the hotel is Child hotel or single hotel----------------------------------------
        cur.execute("select parent_id,child_id from sm_chain_hotel_mapping where child_id=%s", [property_id])
        data=cur.fetchall()
        print "length of data" + str(len(data))
        if len(data)==0:
            #database_language_url_parameter = self.language_url_parameter(cur, property_id)
            database_language_url_parameter = data_base_utils.language_url_parameter(cur, property_id)
            print database_language_url_parameter
            self.assertEqual(database_language_url_parameter, jformat.get("languageUrlParameter"))
            ref_json_to_compare['languageUrlParameter'] = database_language_url_parameter
        else:
            database_chain_id = data_base_utils.chain_id(cur,property_id)
            print database_chain_id
            # print jformat.get("chainId")
            # print database_chain_id == jformat.get("chainId")
            self.assertEqual(database_chain_id, jformat.get("chainId"))
            ref_json_to_compare['chainId'] = database_chain_id
     
#------- Checking paymentGatewayFee --------------------------------------
        database_paymentgateway_fee = data_base_utils.fetching_paymentgateway_fee(
            cur, property_id)
        print database_paymentgateway_fee
        self.assertEqual(database_paymentgateway_fee, jformat.get("paymentGatewayFee"))
        ref_json_to_compare['paymentGatewayFee'] = database_paymentgateway_fee

#------- Checking Simplotel Fee ------------------------------------------
        database_simplotel_fee = data_base_utils.fetching_simplotel_fee(cur, property_id)
        print database_simplotel_fee
        self.assertEqual(database_simplotel_fee, jformat.get("simplotelPercentage"))
        ref_json_to_compare['simplotelPercentage'] = database_simplotel_fee

#------- Checking Google Add Percentage ----------------------------------
        database_google_add_percentage = data_base_utils.fetching_google_add_percentage(
            cur, property_id)
        print database_google_add_percentage
        self.assertEqual(database_google_add_percentage, jformat.get("googleAdsPercentage"))
        ref_json_to_compare[
            'googleAdsPercentage'] = database_google_add_percentage

#------- Checking Service Tax Percentage ---------------------------------
        database_service_tax_percentage = data_base_utils.fetching_service_tax_percentage(
            cur, property_id)
        print database_service_tax_percentage
        self.assertEqual(database_service_tax_percentage, jformat.get("serviceTaxPercentage"))
        ref_json_to_compare[
            'serviceTaxPercentage'] = database_service_tax_percentage

#------- Checking Is Before Tax ------------------------------------------
        database_is_before_tax = data_base_utils.fetching_is_before_tax(cur, property_id)
        print database_is_before_tax
        self.assertEqual(database_is_before_tax, jformat.get("beforeTax"))
        ref_json_to_compare['beforeTax'] = database_is_before_tax

#------- Checking minCollectAmountForPGFee -------------------------------
        database_min_coll_amt_for_pg_fee = data_base_utils.min_coll_amt_for_pg_fee(
            cur, property_id)
        print database_min_coll_amt_for_pg_fee
        self.assertEqual(database_min_coll_amt_for_pg_fee, jformat.get("minCollectAmountForPGFee"))
        ref_json_to_compare[
            'minCollectAmountForPGFee'] = database_min_coll_amt_for_pg_fee

#------- Checking Payment Gateway Type -----------------------------------
        database_paymentgateway_type = data_base_utils.paymentgateway_type(
            cur, property_id)
        print database_paymentgateway_type
        self.assertEqual(database_paymentgateway_type, jformat.get("payment_gateway_type"))
        ref_json_to_compare[
            'payment_gateway_type'] = database_paymentgateway_type

#------- Checking Payment Gateway ----------------------------------------
        database_payment_gateway_name = data_base_utils.payment_gateway_name(
            cur, property_id)
        print database_payment_gateway_name
        self.assertEqual(database_payment_gateway_name, jformat.get("payment_gateway"))
        if database_payment_gateway_name == "INSTAMOJO":
        	is_stop_payout_acc = data_base_utils.is_stop_payout(cur, property_id)
        	is_zero_pg_acc = data_base_utils.is_zero_pg_account(cur, property_id)
        	ref_json_to_compare['stopPayouts'] = is_stop_payout_acc
        	ref_json_to_compare['zeroPGAccount'] = is_zero_pg_acc	
        ref_json_to_compare['payment_gateway'] = database_payment_gateway_name

#------- Checking Booking URL----------------------------------------
        database_booking_url = data_base_utils.booking_url(cur, property_id)
        print database_booking_url
        self.assertEqual(database_booking_url, jformat.get("bookingURL"))
        ref_json_to_compare['bookingURL'] = database_booking_url

#------- Checking Booking Engine Type----------------------------------------
        database_booking_engine_type = data_base_utils.booking_engine_type(cur, property_id)
        print database_booking_engine_type
        self.assertEqual(database_booking_engine_type, jformat.get("bookingEngineType"))
        ref_json_to_compare['bookingEngineType'] = database_booking_engine_type

#------- Checking res Button----------------------------------------
        database_res_btn_name = data_base_utils.button_name(cur, property_id)
        print database_res_btn_name
        self.assertEqual(database_res_btn_name, jformat.get("resBtnName"))
        ref_json_to_compare['resBtnName'] = database_res_btn_name

#------- Checking open in new window----------------------------------------
        database_open_in_new_window = data_base_utils.open_new_window(cur, property_id)
        print database_open_in_new_window
        self.assertEqual(database_open_in_new_window, jformat.get("openInNewWindow"))
        ref_json_to_compare['openInNewWindow'] = database_open_in_new_window

#------- Checking check in date----------------------------------------
        database_checkin_date = data_base_utils.checkin_date(cur, property_id)
        print database_checkin_date
        self.assertEqual(database_checkin_date, jformat.get("checkInDate"))
        ref_json_to_compare['checkInDate'] = database_checkin_date

#------- Checking show promocode----------------------------------------
        database_show_promocode = data_base_utils.show_promocode(cur, property_id)
        print database_show_promocode
        self.assertEqual(database_show_promocode, jformat.get("showPromocode"))
        ref_json_to_compare['showPromocode'] = database_show_promocode

#------- Checking Address Collection----------------------------------------
        database_collect_address = data_base_utils.collect_address(cur, property_id)
        print database_collect_address
        self.assertEqual(database_collect_address, jformat.get("collectAddress"))
        ref_json_to_compare['collectAddress'] = database_collect_address

#------- Checking length of stay----------------------------------------
        database_length_of_stay = data_base_utils.length_of_stay(cur, property_id)
        print database_length_of_stay
        self.assertEqual(database_length_of_stay, jformat.get("lengthOfStay"))
        ref_json_to_compare['lengthOfStay'] = database_length_of_stay

# #------- Checking language url parameter----------------------------------------
#         database_language_url_parameter = self.language_url_parameter(cur, property_id)
#         print database_language_url_parameter
#         print jformat.get("languageUrlParameter")
#         print database_language_url_parameter == jformat.get("languageUrlParameter")
#         ref_json_to_compare['languageUrlParameter'] = database_language_url_parameter

#------- Checking default adult----------------------------------------
        database_default_adult = data_base_utils.default_adult(cur, property_id)
        print database_default_adult
        self.assertEqual(database_default_adult, jformat.get("adults"))
        ref_json_to_compare['adults'] = database_default_adult

#------- Checking default children----------------------------------------
        database_default_children = data_base_utils.default_children(cur, property_id)
        print database_default_children
        self.assertEqual(database_default_children, jformat.get("children"))
        ref_json_to_compare['children'] = database_default_children

#------- Checking max adult----------------------------------------
        database_max_adult = data_base_utils.max_adult(cur, property_id)
        print database_max_adult
        self.assertEqual(database_max_adult, jformat.get("maxAdults"))
        ref_json_to_compare['maxAdults'] = database_max_adult

#------- Checking max children----------------------------------------
        database_max_children = data_base_utils.max_children(cur, property_id)
        print database_max_children
        self.assertEqual(database_max_children, jformat.get("maxChildren"))
        ref_json_to_compare['maxChildren'] = database_max_children

#------- Checking property id----------------------------------------
        database_property_id = data_base_utils.property_Id(cur, property_id)
        print database_property_id
        self.assertEqual(database_property_id, jformat.get("propertyId"))
        ref_json_to_compare['propertyId'] = database_property_id
        print ref_json_to_compare
        # with open('/tmp/data5.json', 'w') as outfile:
        #     json.dump(ref_json_to_compare, outfile)
        #print jformat == ref_json_to_compare 
        #self.assertEqual(jformat, ref_json_to_compare)                         

if __name__ == "__main__":
    unittest.main()
